import Vue from 'vue'
import VueRouter from 'vue-router'

// import HomeManager from '@/views/HomePage.vue'
// import UserManager from '@/views/UserManager.vue'
import MainPage from '@/views/MainPage.vue'
// import MallManager from '@/views/MallManager'
// import TestPage01 from '@/views/TestPage01'
// import TestPage02 from '@/views/TestPage02'
import LoginPage from '@/views/LoginPage'
import Cookie from 'js-cookie'

Vue.use(VueRouter)

// 1. 定义路由组件
// 2. 定义路由-路由与组件的映射
// 3. 创建router实例bing

const routes = [
    {
        path: '/',
        name: 'mainPage',
        redirect: '/home',
        component: MainPage,
        // 子路由 - 嵌套路由
        children: [ 
            // {
            //     path: "home",
            //     name: 'home',
            //     component: HomeManager
            // },
            // {
            //     path: "user",
            //     name: 'user',
            //     component: UserManager
            // },
            // {
            //     path: "mall",
            //     name: 'mall',
            //     component: MallManager
            // },
            // {
            //     path: '/other/page01',
            //     name: 'page01',
            //     component: TestPage01
            // },
            // {
            //     path: '/other/page02',
            //     name: 'page02',
            //     component: TestPage02
            // },
        ]
    },
    {
        path: '/login',
        name: 'login',
        component: LoginPage
    }
]

const router = new VueRouter({
    routes // (缩写) 相当于 routes: routes
})

// 添加全局前置导航守卫
router.beforeEach((to, from, next) => {
    const token = Cookie.get('token')
    if ( !token && to.name !== 'login' ) { // token不存在，当前未登录 => 跳转到登录页
        next({ name: 'login'})
    } else if (token && to.name === 'login') { // token存在，已登录 => 跳转到原路径
        // if(from.name !== 'home') {
        //     next({ name: from.name})
        // }else {
        //     next({ name: 'home'})
        // }
        next({ name: 'home'})
    } else {
        next()
    }

})


// 对外暴露
export default router


