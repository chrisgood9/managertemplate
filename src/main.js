import Vue from 'vue'
import App from './App.vue'
import 'element-ui/lib/theme-chalk/index.css';
import ElementUI from 'element-ui'
import router from './router';
import store from './store';
import '@/api/mock'

Vue.config.productionTip = false

// 全局引入 ElementUI （也可以选择按需引入）
Vue.use(ElementUI)

new Vue({
  router,
  store,
  render: h => h(App),
  created() {
    store.commit('addMenu', router)
  }
}).$mount('#app')
