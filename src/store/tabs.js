import Cookies from "js-cookie"

export default {
    state: {
        isCollapse: false, // 控制菜单的展开还是收起
        tabsList: [ // 面包屑数据
            {
                path: '/',
                name: 'home',
                label: '首页',
                icon: 's-home',
                url: 'Home/Home',
            }
        ], 
        menu: [] // 菜单数据
    },
    mutations: {
        // 修改菜单展开收起的方法
        collapseMenu(state) {
            state.isCollapse = !state.isCollapse
        },
        // 更新面包屑数据
        selectMenu(state, val) {
            // console.log(val)
            // 判断添加的数据是否是首页 => 首页不做任何操作
            if (val.name !== 'home') {
                // findIndex : 如果符合条件则返回当前数组的索引，否则返回-1
                const index = state.tabsList.findIndex(item => item.name === val.name)
                // 不存在则添加
                if (index === -1) {
                    state.tabsList.push(val)
                }
            }
        },
        // 删除指定的tag数据
        closeTags(state, index) {
            // 获取当前tag的索引值，再从tabsList中删除掉
            // const index = state.tabsList.findIndex(val => val.name === item.name)
            state.tabsList.splice(index, 1) // splice(索引值, 删除的个数)
        },
        // 设置menu的数据并保存到缓存中(cookie)
        setMenu(state, val) {
            state.menu = val
            Cookies.set('menuData',JSON.stringify(val))
        },
        // 组装动态路由
        addMenu(state, router) {
            //首先判断缓存中是否存在，不存在则退出，存在则进行组装路由
            if (!Cookies.get('menuData')) return 
            const menu =  JSON.parse(Cookies.get('menuData'))
            state.menu = menu
            // 组装动态路由的数据
            const menuArrays = []
            menu.forEach(item => {
                if (item.children) { // 存在子路由
                    item.children = item.children.map(item => {
                        item.component = () => import(`@/views/${item.url}`)
                        return item
                    })
                    menuArrays.push(...item.children)
               } else {
                    item.component = () => import(`@/views/${item.url}`)
                    menuArrays.push(item)
               }
            })
            console.log('menuArrays', menuArrays)
            // 路由的动态添加
            menuArrays.forEach(item => {
                router.addRoute('mainPage', item)
            })
        }
    }
}