import request from "@/utils/request"

// 这里仅仅是简单的使用-可以拆分成不同的js文件

// 请求首页数据
export const getData = () => {
    return request.get('/home/getData')
}

// 用户数据
export function getUser(param){
    return request({
        url: '/user/get',
        method: 'get',
        params: param
    })
}
export function addUser(data) {
    return request({
        url: '/user/add',
        method: 'post',
        data: data
    })
}
export function delUser(data) {
    return request({
        url: '/user/del',
        method: 'post',
        data: data
    })
}
export function updateUser(data) {
    return request({
        url: '/user/update',
        method: 'post',
        data: data
    })
}

// ****************************************************
// 菜单
export function getMenu(data) {
    return request({
        url: '/permission/getMenu',
        method: 'post',
        data: data
    })
}




